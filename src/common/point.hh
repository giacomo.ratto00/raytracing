#ifndef __COMMON_HH__
#define __COMMON_HH__

#include <SFML/System/Vector3.hpp>
#include <immintrin.h>

class Point : public sf::Vector3<double> {
  public:
	// const double unused = 0.0;
	using sf::Vector3<double>::Vector3;
	Point(sf::Vector3<double> const &v) : sf::Vector3<double>(v) {}
	using sf::Vector3<double>::operator=;

};
// } __attribute__((aligned((16))));

static inline double dot1(Point const &a, Point const &b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

#define dot(x, y) dot1(x, y)

#endif
