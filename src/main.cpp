#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Time.hpp>
#include <chrono>
#include <cmath>
#include <cstring>
#include <iostream>
#include <limits>
#include <pthread.h>
#include <random>
#include <unistd.h>
#include <utility>

#ifdef __AVX2__
#include <immintrin.h>
#endif

#include "common/point.hh"

#define MAX_BOUNCES 8

double schlick_approx(double n1, double n2, double cos) {
	double r0 = (n1 - n2) / (n1 + n2);
	r0 *= r0;

	return r0 + (1 - r0) * std::pow(1 - cos, 5);
}

class Profile {
	char const *msg;
	std::chrono::steady_clock::time_point t1;

  public:
	Profile(char const *msg) : msg(msg) {
		t1 = std::chrono::steady_clock::now();
	}
	~Profile() {
		auto t2 = std::chrono::steady_clock::now();
		auto time =
			std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
		std::cerr << msg << ": " << time << std::endl;
	}
};

typedef sf::Vector3<double> Color;

Color component_mul(Color const &a, Color const &b) {
	return Color(a.x * b.x, a.y * b.y, a.z * b.z);
}

sf::Color color_to_sf(Color const &c) {
	return sf::Color((sf::Uint8)(std::clamp(c.x, 0.0, 1.0) * 255.0),
					 (sf::Uint8)(std::clamp(c.y, 0.0, 1.0) * 255.0),
					 (sf::Uint8)(std::clamp(c.z, 0.0, 1.0) * 255.0), 255);
}

#define BLACK (Color(0.0, 0.0, 0.0))
#define WHITE (Color(1.0, 1.0, 1.0))
#define RED (Color(1.0, 0.01, 0.01))
#define GREEN (Color(0.01, 1.0, 0.01))
#define BLUE (Color(0.01, 0.01, 1.0))



void normalize(Point &x) { x /= std::sqrt(dot(x, x)); }

#define LEN(x) (sizeof(x) / sizeof(x[0]))

class Sphere {
  public:
	Point center;
	double radius;

	std::pair<double, double> hit(Point const &origin, Point const &dir) const {
		double r = this->radius;
		Point CO = origin - this->center;

		double a = dot(dir, dir);
		double b_half = dot(CO, dir);
		double c = dot(CO, CO) - r * r;
		double delta = std::sqrt(b_half * b_half - a * c);

		double t1 = (-b_half - delta) / a;
		double t2 = (-b_half + delta) / a;

		return std::pair{t1, t2};
	};
} __attribute__((aligned(32)));

struct Material {
	Color color;
	double shiny_exponent;
	double reflection;
	double ior;
};

static const Sphere spheres[] = { //
	Sphere{
		.center = {-2, -1, 3},
		.radius = 1,
	},
	Sphere{
		.center = {2, 1, 5},
		.radius = 1,
	},
	Sphere{
		.center = {0, 0, 4},
		.radius = 1,
	},
	Sphere{
		.center = {0, 0, 6},
		.radius = 1,
	},
	Sphere{
		.center = {4, 0, 4},
		.radius = 1,
	},
	Sphere{
		.center = {0, 3, 6},
		.radius = 1,
	},
	Sphere{
		.center = {0, -1001, 0},
		.radius = 1000,
	}};

static Material materials[] = {
	Material{.color = RED, .shiny_exponent = -1, .reflection = .0},
	Material{
		.color = GREEN, .shiny_exponent = 1000, .reflection = 1, .ior = 1.5},
	Material{.color = WHITE, .shiny_exponent = 20, .reflection = 1, .ior = 1.5},
	Material{.color = BLUE, .shiny_exponent = 10, .reflection = 1, .ior = 1.5},
	Material{.color = BLUE, .shiny_exponent = 10, .reflection = 1, .ior = 1.5},
	Material{.color = BLUE, .shiny_exponent = 10, .reflection = 1, .ior = 1.5},
	Material{.color = {.8, .8, 0},
			 .shiny_exponent = 0,
			 .reflection = .3,
			 .ior = 1.5},
};

struct PointLight {
	Color intensity;
	Point position;
};

struct DirectionalLight {
	double intensity;
	Point direction;
};

static const Color ambient_light(.1, .1, .1);

static const PointLight point_lights[] = {
	PointLight{.intensity = {.8, .4, .1}, .position = {1, 2.5, 0}},
	PointLight{.intensity = {.1, .4, .8}, .position = {-1, 2.5, 0}}};

void *worker_thread_main(void *p);

class Canvas {
  public:
	struct ThreadData {
		Canvas *canvas;
		pthread_t thread;
		unsigned int number;
	};

	const long width;
	const long height;
	sf::Color *texture;

	const double v_width = 16.0 / 9;
	const double v_height = 1;
	const double v_distance = 1;

	Point camera = {0, 0, 0};
	std::vector<ThreadData> threads{};
	pthread_barrier_t barrier;

	Canvas(long width, long height) : width{width}, height{height} {
		texture = new sf::Color[width * height];
		int cpu_count = sysconf(_SC_NPROCESSORS_ONLN);
		std::cerr << "using " << cpu_count << '\n';
		pthread_barrier_init(&barrier, NULL, cpu_count + 1);
		threads.resize(cpu_count);
		for (int i = 0; i < cpu_count; i++) {
			threads[i].canvas = this;
			threads[i].number = i;
			pthread_create(&threads[i].thread, NULL, worker_thread_main,
						   &threads[i]);
		}
	}

	unsigned long index_at(long x, long y) {
		return (x + width / 2) + (-y - 1 + height / 2) * width;
	}

	void put_pixel(long x, long y, Color c) {
		auto i = index_at(x, y);
		texture[i] = color_to_sf(c);
	}

	Point canvas_to_viewport(double x, double y) {
		return Point(x * v_width / width, y * v_height / height, v_distance);
	}

	Color compute_light(Point const &p, Point const &normal, double shiny) {
		Color intensity = ambient_light;

		Point v = camera - p;
		normalize(v);
		for (auto &light : point_lights) {
			Point l = light.position - p;

			// shadow
			int closest_sphere;
			double distance;
			std::tie(closest_sphere, distance) =
				closest_intersection(p, l, 0.01, 1);
			if (closest_sphere != -1)
				continue;

			normalize(l);
			Point r = 2 * dot(normal, l) * normal - l;
			normalize(r);

			auto diffuse = std::max(0.0, dot(l, normal));
			auto specular =
				shiny >= 0 ? std::max(0.0, std::pow(dot(r, v), shiny)) : 0;
			intensity += light.intensity * (diffuse + specular);
		}

		return intensity;
	}

	std::pair<int, double> closest_intersection(Point const &origin,
												Point const &direction,
												double min_t, double max_t) {
		double closest = std::numeric_limits<double>::infinity();
		int closest_sphere = -1;
		for (int sphere = 0; sphere < LEN(spheres); sphere++) {
			double t1;
			double t2;

			// 🤡
			std::tie(t1, t2) = spheres[sphere].hit(origin, direction);
			if (t1 >= min_t && t1 <= max_t && t1 < closest) {
				closest = t1;
				closest_sphere = sphere;
			}
			if (t2 >= min_t && t2 <= max_t && t2 < closest) {
				closest = t2;
				closest_sphere = sphere;
			}
		}

		return std::pair{closest_sphere, closest};
	}

	std::pair<Sphere const *, double> any_intersection(Point const &origin,
													   Point const &direction,
													   double min_t,
													   double_t max_t) {
		double closest = std::numeric_limits<double>::infinity();
		Sphere const *closest_sphere = nullptr;
		for (auto &sphere : spheres) {
			double t1;
			double t2;

			// 🤡
			std::tie(t1, t2) = sphere.hit(origin, direction);
			if (t1 >= min_t && t1 <= max_t && t1 < closest) {
				closest = t1;
				closest_sphere = &sphere;
				break;
			}
			if (t2 >= min_t && t2 <= max_t && t2 < closest) {
				closest = t2;
				closest_sphere = &sphere;
				break;
			}
		}

		return std::pair{closest_sphere, closest};
	}

	Color trace_ray(Point const &origin, Point const &direction, double min_t,
					double max_t, int bounces) {
		double closest;
		int closest_sphere = -1;
		std::tie(closest_sphere, closest) =
			closest_intersection(origin, direction, min_t, max_t);

		if (closest_sphere == -1) {
			return BLACK;
		}

		auto p = origin + direction * closest;
		auto normal = p - spheres[closest_sphere].center;
		normal /= std::sqrt(dot(normal, normal));
		auto local_color = component_mul(
			materials[closest_sphere].color,
			compute_light(p, normal, materials[closest_sphere].shiny_exponent));

		// local_color += global_illumination();
		double reflection = materials[closest_sphere].reflection;
		const double ior = materials[closest_sphere].ior;

		if (bounces <= 0 || reflection == 0.0)
			return local_color;

		reflection *= schlick_approx(
			1, ior, dot(-direction, normal) / dot(direction, direction));

		// local_color.x = reflection;
		// local_color.y = reflection;
		// local_color.z = reflection;

		auto reflected_color =
			trace_ray(p, reflect_ray(-direction, normal), 0.01,
					  std::numeric_limits<double>::infinity(), bounces - 1);

		local_color = local_color * (1 - reflection);
		reflected_color = reflected_color * reflection;
		return local_color + reflected_color;
		// return local_color;
		// return reflected_color;
	}

	Point reflect_ray(Point const &r, Point const &n) {
		return 2 * dot(r, n) * n - r;
	}

	void render() {
		Profile p{"render time"};
		pthread_barrier_wait(&barrier);
		pthread_barrier_wait(&barrier);
		// for (long int y = -height / 2; y < height / 2; y++) {
		// 	for (long int x = -width / 2; x < width / 2; x++) {
		// 		auto D = canvas_to_viewport(x, y);
		// 		auto color = trace_ray(
		// 			camera, D, 1,
		// 			std::numeric_limits<double>::infinity(),
		// 			3);
		// 		put_pixel(x, y, color);
		// 	}
		// }
	}

	~Canvas() { delete[] texture; }
};

const int width = 1920;
const int height = 1080;

int main() {
	// sf::VideoMode current = sf::VideoMode::getDesktopMode();
	// sf::ContextSettings settings;
	// sf::RenderWindow window(current, "SFML test", sf::Style::Default, settings);
	// unsigned width = current.width, height = current.height;
	// std::cout << x_len << ',' << y_len << std::endl;
	sf::Texture texture{};
	texture.create(width, height);

	sf::Sprite sprite{};
	sprite.setTexture(texture);

	Canvas canvas{width, height};

	canvas.render();
	texture.update((sf::Uint8 *)canvas.texture);
	texture.copyToImage().saveToFile("output.png");

	// while (window.isOpen()) {
	// 	sf::Event event;
	// 	window.draw(sprite);
	// 	window.display();
	// 	while (window.pollEvent(event)) {
	// 		if (event.type == sf::Event::Closed)
	// 			window.close();
	// 		else if (event.type == sf::Event::KeyPressed) {
	// 			Point move(0, 0, 0);
	// 			// double scale = 1;
	// 			double step = 1;
	// 			switch (event.key.code) {
	// 			case sf::Keyboard::A:
	// 				move.x = -step;
	// 				break;
	// 			case sf::Keyboard::S:
	// 				move.y = -step;
	// 				break;
	// 			case sf::Keyboard::W:
	// 				move.y = step;
	// 				break;
	// 			case sf::Keyboard::D:
	// 				move.x = step;
	// 				break;
	// 			case sf::Keyboard::Add:
	// 				move.z = step;
	// 				break;
	// 			case sf::Keyboard::Subtract:
	// 				move.z = -step;
	// 				break;
	// 			default:
	// 				break;
	// 			}
	//
	// 			if (move != Point{0, 0, 0}) {
	// 				canvas.camera += move;
	// 				canvas.render();
	// 				texture.update((sf::Uint8 *)canvas.texture);
	// 			} else {
	// 				// 60 fps
	// 				sf::sleep(sf::milliseconds(1000 / 60));
	// 			}
	// 		}
	// 	}
	// }
}

#define BLOCK_SIZE 32
std::pair<double, double> ssaa[] = {
	{0, 0}, {.25, .25}, {-.25, .25}, {-.25, -.25}, {.25, -.25}};

void *worker_thread_main(void *p) {
	auto data = (Canvas::ThreadData *)p;
	auto canvas = data->canvas;
	const auto thread_count = canvas->threads.size();
	while (1) {
		pthread_barrier_wait(&data->canvas->barrier);
		// do work
		for (long int i = -canvas->height / 2 + data->number * BLOCK_SIZE;
			 i < canvas->height / 2; i += thread_count * BLOCK_SIZE) {
			for (long int y = i; y < i + BLOCK_SIZE && y < canvas->height / 2;
				 y++) {
				for (long int x = -canvas->width / 2; x < canvas->width / 2;
					 x++) {
					Color color(0.0, 0.0, 0.0);
					for (int k = 0; k < LEN(ssaa); k++) {
						double x_d;
						double y_d;
						std::tie(x_d, y_d) = ssaa[k];
						x_d += x;
						y_d += y;
						auto D = canvas->canvas_to_viewport(x_d, y_d);
						color += canvas->trace_ray(
							canvas->camera, D, 1,
							std::numeric_limits<double>::infinity(),
							MAX_BOUNCES);
					}
					color /= ((double)LEN(ssaa));
					canvas->put_pixel(x, y, color);
				}
			}
		}

		pthread_barrier_wait(&data->canvas->barrier);
	}
	return NULL;
}
