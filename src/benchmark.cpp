#include "common/point.hh"
#include <benchmark/benchmark.h>

// Define another benchmark
static void BM_dot1(benchmark::State &state) {
	Point x(1.2, 7.8, -.8);
	for (auto _ : state) {
		double r = dot1(x, x);
		benchmark::DoNotOptimize(r);
	}
}
BENCHMARK(BM_dot1);

BENCHMARK_MAIN();
