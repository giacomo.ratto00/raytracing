SRC_DIR:=./src
BUILD_DIR:=./build

CXX:=g++
CXXFLAGS:=-g -flto -march=native -Ofast -fopt-info-missed=missed.all -Wall -Wpedantic -Wfatal-errors -std=gnu++20
CPPFLAGS:=-I$(SRC_DIR)

BIN_SRC:=$(wildcard $(SRC_DIR)/*.cpp)
BIN_OBJ:=$(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(BIN_SRC:.cpp=.o))
OTHER_SRC:=$(wildcard $(SRC_DIR)/*/*.cpp)
OTHER_OBJ:=$(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(OTHER_SRC:.cpp=.o))
LDLIBS:=-lsfml-graphics -lsfml-window -lsfml-system -pthread -lbenchmark

.DEFAULT_GOAL:=all
BINS:=$(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(basename $(BIN_OBJ)))
.PHONY: all
all: $(BINS)

include $(BIN_OBJ:.o=.mk) $(OTHER_OBJ:.o=.mk)

$(BUILD_DIR) $(BUILD_DIR)/:
	mkdir -p $@

.PRECIOUS: $(BUILD_DIR)/%/
$(BUILD_DIR)/%/:
	mkdir -p $@

.SECONDEXPANSION:
$(BUILD_DIR)/%.mk: $(SRC_DIR)/%.cpp | $$(dir $(BUILD_DIR)/%.mk)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MM -MT"$(@:.mk=.o)" -MF"$@" "$<"

.SECONDEXPANSION:
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp | $$(dir $(BUILD_DIR)/%.o)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

$(BINS): $(BUILD_DIR)/%: $(BUILD_DIR)/%.o $(OTHER_OBJ) | $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDLIBS)

.PHONY: clean
clean:
	rm -fr $(BUILD_DIR)

compile_commands.json: Makefile
	make clean && bear -- make

.PHONY: perf-stat
perf-stat: build/main
	perf stat -e task-clock:u,cycles:u,stalled-cycles-backend:u,stalled-cycles-frontend:u $<

.PHONY: perf-record
perf-record: build/main
	# perf record --call-graph dwarf -F 199 $<
	perf record -F max -e stalled-cycles-backend:u $<

.PHONY: perf-topdown
perf-topdown: build/main
	perf stat --topdown -a -- $<

